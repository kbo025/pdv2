import Vue from 'vue'
import VueRouter from 'vue-router'
import ClienteList from '../views/ClienteList.vue'
import MotoristaList from '../views/MotoristaList.vue'
import MesaList from '../views/MesaList.vue'
import ProdutoList from '../views/ProdutoList.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/clientes',
    name: 'clientes',
    component: ClienteList
  },
  {
    path: '/motoristas',
    name: 'motoristas',
    component: MotoristaList
  },
  {
    path: '/mesas',
    name: 'mesas',
    component: MesaList
  },
  {
    path: '/produtos',
    name: 'produtos',
    component: ProdutoList
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

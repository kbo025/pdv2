import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const usuario = {
  namespaced: true,
  state: {
    nome: 'Gabriel',
    empresa: 'kbyte soluciones',
    logo: require('../assets/img/logo.png')
  },
  mutations: { },
  actions: { },
  getters: { }
}

const ticket = {
  namespaced: true,
  state: { },
  mutations: { },
  actions: { },
  getters: { }
}

const clientes = {
  namespaced: true,
  state: { },
  mutations: { },
  actions: { },
  getters: { }
}

const mesas = {
  namespaced: true,
  state: { },
  mutations: { },
  actions: { },
  getters: { }
}

const motoristas = {
  namespaced: true,
  state: { },
  mutations: { },
  actions: { },
  getters: { }
}

const produtos = {
  namespaced: true,
  state: { },
  mutations: { },
  actions: { },
  getters: { }
}

export default new Vuex.Store({
  modules: {
    usuario,
    ticket,
    clientes,
    mesas,
    motoristas,
    produtos
  }
})
